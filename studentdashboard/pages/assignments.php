<?php include_once('header.php'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Assignments</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Upload the zip file
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" enctype="multipart/form-data" method="post">
                                        <div class="form-group">
                                            <label>File input</label>
                                            <input type="file" name="file"></br>
                                            <button type="submit" class="btn btn-default" name="upload">Submit Assignment</button>
                                        </div>
										<?php


if(isset($_POST['upload'])){


    $array = explode(".",$_FILES['file']['name']);

    $filename = $array[0];

    $fileextension = strtolower(end($array));

    if($fileextension == "zip"){

        if(is_dir("GradElite/unziped".$filename) == false)
        {

            move_uploaded_file($_FILES['file']['tmp_name'],'Ziped/'.$_FILES['file']['name']);

            $zip = new ZipArchive();

            $zip->open("Ziped/".$_FILES['file']['name']);

            for($num = 0; $num < $zip->numFiles; $num++)
            {

               $fileinfo =  $zip->statIndex($num);

               $zip->extractTo("unZiped/".$filename);

               $zip->close();
}

            echo "<span style='color:green'>"."file has been successfully uploaded! Thank-you"."</span>";           
        }
        else
        {

            echo $filename."has already been unzipped";
        }



    }

    else

    {

        echo "<span style='color:red'>"."Not a vaild file type! Please select .zip files"."</span>";


    }






}

?>
                                        
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
<?php include_once('footer.php'); ?>  
